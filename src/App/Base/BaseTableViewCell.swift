
//
//  BaseTableViewCell.swift
//  BaseProject
//
//  Created by José Manuel Velázquez on 13/04/2019.
//  Copyright © 2019 José Manuel Velázquez. All rights reserved.
//

import Foundation
import UIKit
import SwifterSwift

class BaseTableViewCell: UITableViewCell, BaseCellProtocol {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadStyle() {
        
    }
    
    func loadCell() {
        
    }

}
