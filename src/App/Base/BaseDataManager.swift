//
//  BaseDataManager.swift
//  BaseProject
//
//  Created by José Manuel Velázquez on 12/04/2019.
//  Copyright © 2019 José Manuel Velázquez. All rights reserved.
//

import UIKit
import Alamofire

class BaseDataManager: NSObject {
    
    func requestAPI<T:Decodable> (url: String, method: HTTPMethod, parameters: [String:Any], headers: HTTPHeaders, success: @escaping (_ result: T?) -> (), failure: @escaping (_ error: Error?) -> ()) {
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            guard let data = response.data else { return }
            do {
                let decoder = JSONDecoder()
                let responseObj = try decoder.decode(T.self, from: data)
                success(responseObj)
            } catch let error {
                print(error)
                failure(error)
            }
        }
    }
}
